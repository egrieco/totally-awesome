+++
title = "Authors"
description = "The authors of the blog articles."
date = 2021-11-14T22:09:31-07:00
updated = 2021-11-14T22:09:31-07:00
draft = false

# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.author_pages]
"elio-grieco" = "authors/elio-grieco.md"
+++

The authors of the blog articles.
