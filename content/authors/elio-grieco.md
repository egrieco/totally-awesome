+++
title = "Elio Grieco"
description = "Founder of egx.org"
date = 2021-11-14T22:05:34-07:00
updated = 2021-11-14T22:05:34-07:00
draft = false
+++

Founder of [egx.org](https://egx.org).

[@egrieco](https://github.com/egrieco)
