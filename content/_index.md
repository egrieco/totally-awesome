+++
title = "Totally Awesome"

# The homepage contents
[extra]
lead = 'The totality of awesome-* git repos and curated knowledge.'
url = "/docs/"
url_button = "Get started"
repo_url = "https://gitlab.com/egrieco/totally-awesome"

[[extra.list]]
title = "Teaching Web Programming"
content = "Resources for teaching basic web design."
link = "/docs/education/teaching-web-programming/"

[[extra.list]]
title = "Web Scraping"
content = "Tools and techniques to help the web remember."
link = "/docs/world-wide-web/web-scraping/"
+++
