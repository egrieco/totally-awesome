+++
title = "Cybersecurity"
description = "General cybersecurity resources and awesome repos."
date = 2021-11-17T21:51:55-07:00
updated = 2021-11-17T21:51:55-07:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "General cybersecurity resources and awesome repos."
toc = true
top = false
+++

## !!!SECURITY RESEARCH WARNING!!!

**WARNING**: Some repos linked below may contain malware! Do not clone or click randomly! Bad things are likely to happen to your computer or you if you are carless.

I'll try to mark especially dangerous repos as such, but it's best to always exercise caution when working in cybersecurity.

### Personal Security and Checklists

* [macOS-Security-and-Privacy-Guide](https://github.com/drduh/macOS-Security-and-Privacy-Guide)
* [osx-security-awesome](https://github.com/kai5263499/osx-security-awesome)
* [security-checklist](https://github.com/brianlovin/security-checklist)
* [personal-security-checklist](https://github.com/Lissy93/personal-security-checklist)
* [osx-and-ios-security-awesome](https://github.com/ashishb/osx-and-ios-security-awesome)
* [Awesome-Application-Security-Checklist](https://github.com/MahdiMashrur/Awesome-Application-Security-Checklist)

### Education and Best Practices

* [Free-Security-eBooks](https://github.com/Hack-with-Github/Free-Security-eBooks)
* [awesome-linux-android-hacking](https://github.com/pfalcon/awesome-linux-android-hacking)
* [awesome-serverless-security](https://github.com/puresec/awesome-serverless-security)
* [awesome-container-security](https://github.com/kai5263499/awesome-container-security)
* [awesome-cloud-security](https://github.com/Funkmyster/awesome-cloud-security)
* [awesome-security-audits](https://github.com/pomerium/awesome-security-audits)
* [awesome-sdn-security](https://github.com/lopezalvar/awesome-sdn-security)
* [awesome-rails-security](https://github.com/0xedward/awesome-rails-security)
* [awesome-frontend-security](https://github.com/rustcohlnikov/awesome-frontend-security)
* [awesome-bluetooth-security](https://github.com/engn33r/awesome-bluetooth-security)
* [awesome-aws-security](https://github.com/coffeewithayman/awesome-aws-security)
* [awesome-security](https://github.com/sbilly/awesome-security)
* [awesome-web-security](https://github.com/qazbnm456/awesome-web-security)
* [awesome-security-trivia](https://github.com/qazbnm456/awesome-security-trivia)

### Forensics and Reversing

* [awesome-forensics](https://github.com/Cugu/awesome-forensics)
* [awesome-reversing](https://github.com/tylerha97/awesome-reversing)
* [awesome-ml-for-cybersecurity](https://github.com/jivoi/awesome-ml-for-cybersecurity)
* [awesome-yara](https://github.com/InQuest/awesome-yara)
* [awesome-honeypots](https://github.com/paralax/awesome-honeypots)
* [awesome-cybersecurity-blueteam](https://github.com/meitar/awesome-cybersecurity-blueteam)
* [awesome-pcaptools](https://github.com/caesar0301/awesome-pcaptools)
* [awesome-iocs](https://github.com/sroberts/awesome-iocs)
* [image-unshredding](https://github.com/robinhouston/image-unshredding): using simulated annealing to reconstruct photographs whose columns have been shuffled

### Hacking and Pentesting

* [Awesome-Hacking-Resources](https://github.com/vitalysim/Awesome-Hacking-Resources)
* [Awesome-Hacking](https://github.com/Hack-with-Github/Awesome-Hacking)
* [awesome-pentest-cheat-sheets](https://github.com/coreb1t/awesome-pentest-cheat-sheets)
* [awesome-malware-analysis](https://github.com/rshipp/awesome-malware-analysis)
* [awesome-hacking](https://github.com/carpedm20/awesome-hacking)
* [awesome-pentest](https://github.com/enaqx/awesome-pentest)
* [awesome-cve-poc](https://github.com/qazbnm456/awesome-cve-poc)

### OSINT

* [awesome-osint](https://github.com/jivoi/awesome-osint): A curated list of amazingly awesome OSINT
* [osint-tools](https://github.com/HowToFind-bot/osint-tools): OSINT tools catalog
* [OSINTBookmarks](https://github.com/5nacks/OSINTBookmarks): OSINT Bookmarks for Firefox / Chrome / Edge / Safari
* [Awesome-Telegram-OSINT](https://github.com/ItIsMeCall911/Awesome-Telegram-OSINT): A Curated List of Awesome Telegram OSINT Tools, Sites & Resources

#### OSINT Tools

* [sn0int](https://github.com/kpcyrd/sn0int): Semi-automatic OSINT framework and package manager
* [theHarvester](https://github.com/laramies/theHarvester): E-mails, subdomains and names Harvester
* [check-if-email-exists](https://github.com/reacherhq/check-if-email-exists): Check if an email address exists without sending any email, written in Rust.
* [whatsapp-verify](https://github.com/AbhishekBiswal/whatsapp-verify): Check if a phone number exists on Whatsapp

### Industrial Security

* [awesome-industrial-control-system-security](https://github.com/hslatman/awesome-industrial-control-system-security)
* [ICS-Security-Tools](https://github.com/ITI/ICS-Security-Tools)
