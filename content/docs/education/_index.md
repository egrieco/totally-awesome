+++
title = "Education"
description = "Resources for educators, education reform, and learning to code."
date = 2021-11-16T15:52:37-07:00
updated = 2021-11-16T15:52:37-07:00
sort_by = "weight"
weight = 1
template = "docs/section.html"
+++
