+++
title = "Teaching Web Programming"
description = "Resources for teaching basic web design."
date = 2021-11-16T16:36:13-07:00
updated = 2021-11-16T16:36:13-07:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Resources for teaching basic web design."
toc = true
top = false
+++

## Background

Teaching web design and development has moved from relatively simple in the 1990s to about as complex as full on backend programming.

Building a modern website requires, at a minimum, learning the following languages:

* HyperText Markup Language (HTML) - To give the page structure.
* Cascading StyleSheets (CSS) - To control the look of the page, including animation.

For anything more than a trivial site, the following may also be required:

* JavaScript - To add interactivity to the page.
* Ruby/Python/Elixir/JavaScript/etc. - To add backend functionality, whether on traditional servers or via serverless/lambda functions.
* JSON/YAML/TOML/etc. - To exchange data with Application Programming Interfaces (APIs).

## Resources

While professional programmers tend to love massive collections of resources, students need to be slowly introduced to research methods and how to successfully navigate the mountains of data common to software design. The essential resources below should be just about enough for a brief introductory class.

### Essential Resources

* [DevDocs API Documentation](https://devdocs.io/): Excellent reference for most programming languages with fast search.
* [HTML & CSS Is Hard](https://www.internetingishard.com/html-and-css/): A friendly web development tutorial that practices what it preaches.
* [Whitespace – A List Apart](https://alistapart.com/article/whitespace/): Essential reading for web or general typographic design.

### Basic Resources

* [awesome-html5](https://github.com/diegocard/awesome-html5): A curated list of awesome HTML5 resources
* [awesome-css-learning](https://github.com/micromata/awesome-css-learning): A tiny list limited to the best CSS Learning Resources
* [css-group/awesome-css](https://github.com/awesome-css-group/awesome-css): A curated contents of amazing CSS :)
* [awesome-web-accessibility](https://github.com/bhaskarmac/awesome-web-accessibility): A collection of accessibility related resources.

### Design Resources

* [awesome-web-design](https://github.com/nicolesaidy/awesome-web-design): A curated list of awesome resources for digital designers.
* [awesome-design](https://github.com/oscarotero/awesome-design) (oscarotero): A collection of open resources for web designers.
* [awesome-design](https://github.com/troyericg/awesome-design) (troyericg): An ever-growing/changing collection of tools, resources, tutorials, guidelines and examples.
* [awesome-motion-design-web](https://github.com/lucasmaiaesilva/awesome-motion-design-web): A list of links for using motion design on the Web.

### Tooling

* [awesome-no-login-web-apps](https://github.com/aviaryan/awesome-no-login-web-apps): Awesome (free) web apps that work without login
* [awesome-web-online-tools](https://github.com/uretgec/awesome-web-online-tools): Many many useful Web Online Tools For Web Developers & Programmers
* [awesome-web](https://github.com/vietdien2005/awesome-web): A curated list of awesome resources for web developers.

## Static Analysis

It's possible to automatically check the correctness of certain aspects of HTML and CSS code. This makes it possible to automate many aspects of grading freeform projects.

### Validators, Linters, and Formatters

* [HTML-validate](https://html-validate.org/) ([vscode extension](https://gitlab.com/html-validate/vscode-html-validate)): Offline HTML5 validator. Validates either a full document or a smaller (incomplete) template, e.g. from an AngularJS or Vue.js component.
* [HTML Tidy](https://www.html-tidy.org/) ([source code](https://github.com/htacg/tidy-html5)): The granddaddy of HTML tools, with support for modern standards.
* [stylelint](https://github.com/stylelint/stylelint) ([vscode extension](https://github.com/stylelint/vscode-stylelint)): A mighty, modern linter that helps you avoid errors and enforce conventions in your styles.
* [prettier](https://github.com/prettier/prettier) [(vscode extension)](https://github.com/prettier/prettier-vscode): Prettier is an opinionated code formatter.
* [mlc](https://github.com/becheran/mlc): Check for broken links in markup files.

### Querying and Search

* [htmlq](https://github.com/mgdm/htmlq): Like jq, but for HTML.
* [ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore.
* [web-grep](https://github.com/cympfh/web-grep): Allows querying web pages via templates.
