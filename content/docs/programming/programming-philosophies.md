+++
title = "Programming Philosophies"
description = "Different ways of thinking about priorities in programming."
date = 2021-11-17T16:11:04-07:00
updated = 2021-11-17T16:11:04-07:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Different ways of thinking about priorities in programming."
toc = true
top = false
+++

* [The Three Virtues of a Great Programmer](https://web.cs.kent.edu/~whaverst/Edu/Courses/Shared/wall_virtues.html) ([long version](https://betterprogramming.pub/here-are-the-three-virtues-of-good-programmers-e561e061ea19))
* [Pony Philosophy](https://www.ponylang.io/discover/#the-pony-philosophy)
* [Zen of Nim](https://nim-lang.org/blog/2021/11/15/zen-of-nim.html)
