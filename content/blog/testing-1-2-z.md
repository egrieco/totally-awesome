+++
title = "Testing, 1, 2, Z"
description = "Testing out Zola for the organization of Awesome-* Repos."
date = 2021-11-16T10:16:44-07:00
updated = 2021-11-16T10:16:44-07:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["Elio Grieco"]

[extra]
lead = "Curated knowledge is powerful. Finding it is the hard part."
+++

The new home of curated knowledge on the web are the [`awesome-*` repos on GitHub](https://github.com/topics/awesome). Though they skew toward tech, a surprisingly wide variety of topics are represented in the currently 4,715 available repos in this GitHub topic.

The problem is that pointing people toward this resource isn't enough. Most non-programmers are not used to wading through the mountains of knowledge that software developers and data scientists wrangle every day. This website is an early attempt to cover the totality of the awesome repos that I find most useful, and hopefully make them a bit easier to share with others.

Secondarily, it's an excuse to try out [Zola](https://www.getzola.org/), a new [static](https://jamstack.org/generators/) [site](https://staticschool.com/) [generator](https://staticsitegenerators.net/) in [Rust](https://www.rust-lang.org/). I've used a variety of static site generator over the years. They are perfect for creating high performance, secure websites where a small group needs to make changes but most users are only viewing the content.

While [Middleman](https://middlemanapp.com/) has served me well, I'd like something that collaborators can install via a single binary. I've also been coding in Rust for a little while and am quite happy with the code quality and speed of most projects in the [Rust ecosystem](https://lib.rs/). Thus Zola is a perfect fit for this exercise.

Why not [Cobalt](https://cobalt-org.github.io/) or [mdBook](https://rust-lang.github.io/mdBook/)? Well, Zola seems to be a touch more popular than Cobalt and more general purpose than mdBook. If it doesn't meet my needs, it's nice to know that there are other options for future projects.
